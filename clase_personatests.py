from clase_Persona import Persona, Paciente, Medico
import unittest
class TestPersona(unittest.TestCase):

    def test_init(self):
        persona1 = Persona("Juan", "Pérez", "1980-01-01", "12345678A")
        self.assertEqual(persona1.getNombre(), "Juan")
        self.assertEqual(persona1.getApellidos(), "Pérez")
        self.assertEqual(persona1.getFecha_de_nacimineto(), "1980-01-01")
        self.assertEqual(persona1.getDNI(), "12345678A")

    def test_setNombre(self):
        persona1 = Persona("María", "Gómez", "1990-02-02", "87654321B")
        persona1.setNombre("Ana")
        self.assertEqual(persona1.getNombre(), "Ana")

    def test_setApellidos(self):
        persona1 = Persona("Pedro", "Martínez", "2000-03-03", "98765432C")
        persona1.setApellidos("López")
        self.assertEqual(persona1.getApellidos(), "López")

    def test_setFecha_de_nacimineto(self):
        persona1 = Persona("Laura", "Fernández", "2010-04-04", "098765432D")
        persona1.setFecha_de_nacimineto("2011-05-05")
        self.assertEqual(persona1.getFecha_de_nacimineto(), "2011-05-05")

    def test_setDNI(self):
        persona1 = Persona("David", "García", "2020-06-06", "109876543E")
        persona1.setDNI("210987654F")
        self.assertEqual(persona1.getDNI(), "210987654F")

class TestPaciente(unittest.TestCase):
    def setUp(self):
        self.paciente = Paciente("Ana", "López", "2000-03-03", "98765432C", "Historial médico de la paciente")

    def test_ver_historial_clinico(self):
       self.assertEqual(self.paciente.ver_historial_clinico(), "Historial médico de la paciente")

class TestMedico(unittest.TestCase):
    def setUp(self):
        self.medico = Medico("Pedro", "Martínez", "2010-04-04", "098765432D", "Cardiología", "Lista de citas del médico")
    def test_ver_citas_programadas(self):
        self.assertEqual(self.medico.ver_citas_programadas(), "Lista de citas del médico")


if __name__ == "__main__":
    unittest.main()
