class Persona:
    def __init__(self, nombre, apellidos, fecha_de_nacimineto, dni):
        self.__nombre = nombre
        self.__apellidos = apellidos
        self.__fecha_de_nacimineto = fecha_de_nacimineto
        self.__dni = dni
    def setNombre(self,nuevo_nombre):
        self.__nombre = nuevo_nombre
    def setApellidos(self,nuevos_apellidos):
        self.__apellidos = nuevos_apellidos
    def setFecha_de_nacimineto(self,nueva_fecha_de_nacimineto):
        self.__fecha_de_nacimineto = nueva_fecha_de_nacimineto
    def setDNI(self,nuevo_dni):
        self.__dni = nuevo_dni

    def getNombre(self):
        return self.__nombre
    def getApellidos(self):
        return self.__apellidos
    def getFecha_de_nacimineto(self):
        return self.__fecha_de_nacimineto
    def getDNI(self):
        return self.__dni

    def __str__(self):
        return "Nombre: {name}\nApellidos: {surnames}\nFecha de nacimiento: {date}\nDNI: {id}".format(name=self.__nombre, surnames=self.__apellidos, date=self.__fecha_de_nacimineto, id=self.__dni)
class Paciente(Persona):
    def __init__(self, nombre, apellidos, fecha_de_nacimineto, dni, historial_clinico):
        super().__init__(nombre, apellidos, fecha_de_nacimineto, dni)
        self.__historial_clinico = historial_clinico

    def ver_historial_clinico(self):
        print("Historial clínico: {ch}".format(ch=self.__historial_clinico))
        return self.__historial_clinico
class Medico(Persona):
    def __init__(self, nombre, apellidos, fecha_de_nacimineto, dni, especialidad, citas):
        super().__init__(nombre, apellidos, fecha_de_nacimineto, dni)
        self.__especialidad = especialidad
        self.__citas = citas

    def ver_citas_programadas(self):
        print("Citas programadas {pc}".format(pc=self.__citas))
        return self.__citas